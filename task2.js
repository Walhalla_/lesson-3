//1

let x = +prompt(`Enter any number`);
if (x % 3 == 0 && x % 5 == 0) {
 console.log(`x is divisible by 3 and 5`);
}
else {
 console.log(`x is not divisible by 3 and 5`);
}

//2

let y = +prompt(`Enter any number`);
if (y % 2 == 0) {
 console.log(`y is even`);
}
else {
 console.log(`y is odd`);
}

// 3

let arr = [5, 6, 4, 3, 2, 8, 13, 0, 8, -5, -11];
arr.sort((a, b) => a - b);
console.log(arr);

//4

let arr_1 = ['a', 'b', 'c', 'd'];
let arr_2 = ['a', 'b', 'c',];
let arr_3 = ['a', 'd'];
let arr_4 = ['a', 'b', 'k', 'e', 'e'];
let unique_arr = [...new Set(arr_1.concat(arr_2, arr_3, arr_4))];
console.log(unique_arr);

//5

let obj_1 = {
    name: "John",
    age: 25,
    student: true
   }
   
   let obj_2 = {
    age: 25,
    student: true
   }
   function check(obj1, obj2) {
    for (key in obj_2) {
     if (obj_1[key] == obj_2[key]) {
      return true
     }
     else {
      return false
     }
    }
   }
   console.log(check(obj_1, obj_2))